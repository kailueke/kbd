package main

import (
	"fmt"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"
)

const (
	BATTERYPATH = "/sys/class/power_supply/axp20x-battery/capacity"
	// Switch to ../constant_charge_current to allow overcurrent for devices
	CHARGEPATH     = "/sys/class/power_supply/axp20x-usb/input_current_limit"
	BATTERYPATHPPP = "/sys/class/power_supply/rk818-battery/capacity"
	CHARGEPATHPPP  = "/sys/class/power_supply/rk818-usb/input_current_limit"
	KBCHARGEPATH   = "/sys/class/power_supply/ip5xxx-charger/status"
	KBVOLTPATH     = "/sys/class/power_supply/ip5xxx-charger/voltage_now"
	KBNOTCHARGING  = "Discharging"
	// Info registers from Megi's userspace charger controller
	waitTime = 20 * time.Second
	// gsettings stuff
	DBUSADDRESS  = "DBUS_SESSION_BUSS_ADDRESS=unix:path=/run/user/1000/bus"
	XDGRUNTIME   = "XDG_RUNTIME_DIR=/run/user/1000/"
	SCHEMADIR    = "org.gnome.desktop.a11y.applications"
	SCHEMANAME   = "screen-keyboard-enabled"
	ERRKBREMOVED = "no such device or address"
)

func getKBVoltage() (int, error) {
	rawvolt, err := os.ReadFile(KBVOLTPATH)
	if err != nil {
		return 0, err
	}
	volt := strings.TrimSpace(string(rawvolt))
	intvolt, err := strconv.Atoi(volt)
	if err != nil {
		return 0, err
	}
	return intvolt / 1000, nil
}

func KBIsCharging() bool {
	state, err := os.ReadFile(KBCHARGEPATH)
	if err != nil || !strings.Contains(string(state), KBNOTCHARGING) {
		return true // Better to charge slower on error
	}
	return false
}
func main() {
	gsettingsKbdOnCmd := exec.Command("sudo", "-u#1000", DBUSADDRESS, XDGRUNTIME, "gsettings", "set", SCHEMADIR, SCHEMANAME, "true")
	gsettingsKbdOnCmd.Run()
	gsettingsKbdOn := true
	for {
		kbdVoltage, err := getKBVoltage()
		if err != nil {
			if strings.Contains(err.Error(), ERRKBREMOVED) {
				// Kebyoard not attached
				if !gsettingsKbdOn {
					gsettingsKbdOnCmd = exec.Command("sudo", "-u#1000", DBUSADDRESS, XDGRUNTIME, "gsettings", "set", SCHEMADIR, SCHEMANAME, "true")
					err = gsettingsKbdOnCmd.Run()
					if err != nil {
						fmt.Printf("Could not set keyboard on: %+v\n", err)
						out, _ := gsettingsKbdOnCmd.Output()
						fmt.Printf("Output from gsettings: %s", string(out))
					}
					gsettingsKbdOn = true
				}
				time.Sleep(waitTime)
				continue
			} else {
				fmt.Printf("Unexpected error: %+v\n", err)
				time.Sleep(waitTime)
				continue
			}
		}
		// kernel driver return 0 when charge controller is turned off but attached
		if kbdVoltage == 0 {
			if gsettingsKbdOn {
				gsettingsKbdOnCmd = exec.Command("sudo", "-u#1000", DBUSADDRESS, XDGRUNTIME, "gsettings", "set", SCHEMADIR, SCHEMANAME, "false")
				err = gsettingsKbdOnCmd.Run()
				if err != nil {
					fmt.Printf("Could not set keyboard on: %+v\n", err)
					out, _ := gsettingsKbdOnCmd.Output()
					fmt.Printf("Output from gsettings: %s", string(out))
				}
				gsettingsKbdOn = false
			}
			time.Sleep(waitTime)
			continue
		}
		if gsettingsKbdOn {
			gsettingsKbdOnCmd = exec.Command("sudo", "-u#1000", DBUSADDRESS, XDGRUNTIME, "gsettings", "set", SCHEMADIR, SCHEMANAME, "false")
			err = gsettingsKbdOnCmd.Run()
			if err != nil {
				fmt.Printf("Could not set keyboard off: %+v\n", err)
				out, _ := gsettingsKbdOnCmd.Output()
				fmt.Printf("Output from gsettings: %s", string(out))
			}
			gsettingsKbdOn = false
		}
		content, err := os.ReadFile(BATTERYPATH)
		if err != nil {
			fmt.Printf("Error reading %s: %+v\n", BATTERYPATH, err)
			time.Sleep(waitTime)
			continue
		}
		capacity, err := strconv.Atoi(strings.TrimSpace(string(content)))
		if err != nil {
			fmt.Printf("Error parsing battery capacity %s: %s\n", string(content), err)
			time.Sleep(waitTime)
			continue
		}
		content, err = os.ReadFile(CHARGEPATH)
		if err != nil {
			fmt.Printf("Error reading %s: %+v\n", CHARGEPATH, err)
			time.Sleep(waitTime)
			continue
		}
		inputCurrent, err := strconv.Atoi(strings.TrimSpace(string(content)))
		if err != nil {
			fmt.Printf("Error parsing input current %s: %s\n", string(content), err)
			time.Sleep(waitTime)
			continue
		}

		maxCurrent := 1500000
		if KBIsCharging() {
			// Note: this also fires when the keyboard power is disabled
			// meaning the first reading may be wrong after turning the
			// keyboard on.
			maxCurrent = 900000
		}
		expectedCurrent := 500000
		switch {
		case kbdVoltage < 3200:
			expectedCurrent = 500000
		case capacity < 80:
			expectedCurrent = maxCurrent
		case capacity < 90:
			expectedCurrent = 900000
		}

		if inputCurrent != expectedCurrent {
			fmt.Printf("Capacity is %v, input is %v, setting input to %v\n", capacity, inputCurrent, expectedCurrent)
			send := strconv.Itoa(expectedCurrent)
			limitFile, err := os.OpenFile(CHARGEPATH, os.O_RDWR, 0644)
			if err != nil {
				fmt.Printf("Error opening %s, %+v\n", CHARGEPATH, err)
				time.Sleep(waitTime)
				continue
			}
			_, err = limitFile.WriteString(send)
			if err != nil {
				fmt.Printf("Error writing to  %s, %+v\n", CHARGEPATH, err)
				limitFile.Close()
				time.Sleep(waitTime)
				continue
			}
			err = limitFile.Sync()
			if err != nil {
				fmt.Printf("Error syncing %s, %+v\n", CHARGEPATH, err)
				limitFile.Close()
				time.Sleep(waitTime)
				continue
			}
			limitFile.Close()
		}
		time.Sleep(waitTime)
	}

}
